import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
//import '../imports/api/tasks.js';

Meteor.startup(() => {
	Meteor.methods({

		'fbPhotos' : function(query) {
			var coordinates = location().location;
			var page = HTTP.call("GET", "graph.facebook.com/search?q="+query+"&type=place&center="+coordinates.lat+","+coordinates.lng);

			return HTTP.call("GET", "https://graph.facebook.com/"++"/photos/type=tagged");
		}

		'restaurants' : function() {
			var coordinates = location().location;

			const options = {
				"user-key" : "f1756b9a9be6ecba2c87fb55895efb95";
				"Accept" : "application/json";
			}
			return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/geocode?lat="+coordinates.lat+"&lon="+coordinates.lng, options).nearby_restaurants;
		}
		
		'location' : function() {
			return HTTP.call("POST", "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBEBIsj5wDSrav65iys8ws0DSlsAQ56HWY");
		}

	})
  // code to run on server at startup
});

//zomato api key: f1756b9a9be6ecba2c87fb55895efb95
//maps api key: AIzaSyBEBIsj5wDSrav65iys8ws0DSlsAQ56HWY
//facebook api key: 53582d3e8773cf213db5e6ec5488b244

/*Meteor.call('flickrGetPhotos', "hello", function(err, res){
    		if (res) {
    			console.log("GOOD");
    		} else {
    			//console.log(err);
    		}
    	});