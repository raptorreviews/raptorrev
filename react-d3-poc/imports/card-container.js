import React from 'react';
import QueryCard from '/imports/card'

class CardContainer extends React.Component {

	constructor(props) {
		super(props);

	    this.state = {};
	}

	render() {
		return (
			<div>
				{
					this.props.cardsToRender.map((item) => {
						var childCards = item.children.map((i) => {
							return (
								<div key={i.id} className="childCard">
									<QueryCard
										id={i.id}
										e={true}
										type={"child"}
										content={i.val}
										filter={i.filter}
										addSibling={this.props.addSibling}
										addChild={this.props.addChild}
										renameNode={this.props.renameNode}
										setFilter={this.props.setFilter}
										compound={this.props.compound}
									/>
								</div>
							)
						})

						return (
							<div key={item.id}>
								<QueryCard 
									id={item.id}
									e={false}
									content={item.val}
									filter={item.filter}
									addSibling={this.props.addSibling}
									addChild={this.props.addChild}
									renameNode={this.props.renameNode}
									setFilter={this.props.setFilter}
									compound={this.props.compound}
								/>
								{childCards}
							</div>
						);
					})
				}
			</div>
		);
	}
}

export default CardContainer;