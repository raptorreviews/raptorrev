import React from 'react';
import FontIcon from 'material-ui/FontIcon';
import {grey200} from 'material-ui/styles/colors';

class NoTagWarning extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="warning">
				<FontIcon style={{fontSize: "240px"}} className={"material-icons"} color={grey200}>add_circle</FontIcon>
			</div>
		)
	}
}

export default NoTagWarning;