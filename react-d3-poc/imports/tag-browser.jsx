import CardContainer from '/imports/card-container';
import { Meteor } from 'meteor/meteor';
import {darkBlack, grey50, indigo500, indigo50} from 'material-ui/styles/colors';
import update from 'react-addons-update';
import D3Tree from '/imports/d3tree';
import React from 'react';
import {fade} from 'material-ui/utils/colorManipulator';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';

class TagBrowser extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      cards: [{id: 1, val: "Filter 1", filter: "", children: []}],
      cardsToRender: [{ id: 1, val: "Filter 1", filter: "", children: []}],
      treeData: [{"name" : this.props.tag, "parent":"null", "nid":0}],
      cardCount: 1,
      toplevel: 0,
      currFilter: "",
      queryResults: "",
      resultsDialogOpen: false
    };

    this.addSibling = this.addSibling.bind(this);
    this.addChild = this.addChild.bind(this);
    this.computeTreeData = this.computeTreeData.bind(this);
    this.selectTopLevel = this.selectTopLevel.bind(this);
    this.rename = this.rename.bind(this);
    this.setFilter = this.setFilter.bind(this);
    this.compound = this.compound.bind(this);
    this.runQuery = this.runQuery.bind(this);
  }

  componentDidMount() {
    this.computeTreeData();
  }

  runQuery() {
    Meteor.call("runQuery", this.props.tag, this.state.currFilter, this.addResults.bind(this));
  }

  addResults(err, res) {
    this.setState({queryResults: res, resultsDialogOpen: true});
  }

  render() {
    var filterButtonContents = this.state.currFilter;
    var queryResults = this.state.queryResults;

    if(this.state.currFilter != "") {
      filterButtonContents = (
          <RaisedButton onTouchTap={this.runQuery} label={this.state.currFilter} primary={true}/>
        );
    }

    return ( 
      <div className="container">
        <div className="currFilter">
          {filterButtonContents}
        </div>
        <div className="map">
          <D3Tree treeData={this.state.treeData} selector={this.selectTopLevel} selected={this.state.toplevel} />
        </div>
        <div className="cards">
          <CardContainer 
            cardsToRender={this.state.cardsToRender}
            addChild={this.addChild}
            addSibling={this.addSibling}
            renameNode={this.rename} 
            setFilter={this.setFilter}
            compound={this.compound}
          />
        </div>

        <Dialog
          title={"Query Results"}
          modal={false}
          open={this.state.resultsDialogOpen}
          onRequestClose={this.handleResultsClose.bind(this)}>

            {this.state.queryResults} <br/><br/>

        </Dialog>

      </div>
    );
  }

  compound(id) {
    var total = this.recursiveCompound(id, this.state.cards, "");
    if(total == null) return "";
    return total;
  }

  recursiveCompound(id, children, qsf) {

    for(const child of children) {
      if (child.id == id) {
        return (qsf + child.filter);
      } else {
        var cq = this.recursiveCompound(id, child.children, (qsf + child.filter + " AND "));
        if (cq == null) continue;
        return cq;
      }
    }

    return null;

  }

  rename(id, name) {
    var _updateString = this.renameR(id, name, this.state.cards);
    if(_updateString == null) return;

    var newState = update(this.state, {cards: _updateString});
    this.setState(newState, () => this.computeTreeData());
  }

  renameR(id, name, children) {
    for(var i = 0; i < children.length; i++) {
      if(children[i].id == id) {
        return {[i]: {$set: {id: id, val: name, filter:children[i].filter, children: children[i].children}}}
      };

      if(children[i].children.length > 0) {
        var childUpdate = this.renameR(id, name, children[i].children);
        if(childUpdate == null) continue;
        return {[i]: {children: childUpdate}};
      }
    }
    return null;
  }

  setFilter(id, filter) {
    var _updateString = this.setFilterR(id, filter, this.state.cards);
    if(_updateString == null) return;

    var newState = update(this.state, {cards: _updateString});
    this.setState(newState, () => this.computeTreeData());
  }

  setFilterR(id, newFilter, children) {
    for(var i = 0; i < children.length; i++) {
      if(children[i].id == id) {
        return {[i]: {$set: {id: id, val: children[i].val, filter: newFilter, children: children[i].children}}}
      };

      if(children[i].children.length > 0) {
        var childUpdate = this.setFilterR(id, newFilter, children[i].children);
        if(childUpdate == null) continue;
        return {[i]: {children: childUpdate}};
      }
    }
    return null;
  }

  selectTopLevel(id) {
    if(id != 0) {var res = [this.searchR(id, this.state.cards)];} 
    else {var res = this.state.cards;}

    var _newCardsToRender = update(this.state, {cardsToRender: {$set: res}});
    var _newTopLevel = update(_newCardsToRender, {toplevel: {$set: id}});
    var _newFilter = update(_newTopLevel, {currFilter: {$set: this.compound(id)}})
    this.setState(_newFilter);
  }

  searchR(id, children) {
    var res = null;

    for(var i = 0; i < children.length; i++) {
      if(children[i].id == id) return children[i];

      res = this.searchR(id, children[i].children)
      if(res != null) return res;
    }

    return res;
  }

  addChild(parent) {
    var _cards = this.state.cards;
    for(var i = 0; i < _cards.length; i++) {
      if(this.state.cards[i].id == parent) {
        var newChild = {id: (this.state.cardCount + 1),
                        val: "Filter " + (this.state.cardCount + 1),
                        filter: "",
                        children: []}

        var _addChild = update(this.state, {cards: {[i]: {children: {$push: [newChild]}}}});
        var _updateCount = update(_addChild, {cardCount: {$set: this.state.cardCount + 1}});

        this.setState(_updateCount, () => {this.computeTreeData()});
        break;

      } else if (this.state.cards[i].children.length > 0) {
        var _updateString = this.recursiveChild(this.state.cards[i].children, parent);
        if(_updateString != null) {
          var _updatedFamily = update(this.state, {cards: {[i]: {children: _updateString}}});
          var _updatedCount = update(_updatedFamily, {cardCount: {$set: this.state.cardCount + 1}})
          this.setState(_updatedCount, () => this.computeTreeData());
        }
      }
    }
  }

  recursiveChild(arr, parent) {
    for(var i = 0; i < arr.length; i++) {
      if(arr[i].id == parent) {
        var newChild = {id: (this.state.cardCount + 1),
                val: "Filter " + (this.state.cardCount + 1),
                filter: "",
                children: []};

        return {[i]: {children: {"$push": [newChild]}}};
      } else if (arr[i].children.length > 0) {
        var result = this.recursiveChild(arr[i].children, parent);
        if(result != null) {
          return {[i]: {children: result}};
        }
      }
    }

    return null;
  }
  
  addSibling() {
    var _cards = this.state.cards.concat([{id: this.state.cardCount + 1, 
                    val: "Filter " + (this.state.cardCount + 1), filter: "", children:[]}]);

    this.setState({cards: _cards, cardCount: this.state.cardCount + 1}, function(){
      this.computeTreeData();
    });
  }


  computeTreeData() {
    var newTreeData = [{"name" : this.props.tag, "parent":"null", "nid":0 }];
    this.computeTreeDataRecursive(this.state.cards, newTreeData, this.props.tag);

    var newState = update(this.state, {treeData: {$set: newTreeData}});
    this.setState(newState, () => {this.selectTopLevel(this.state.toplevel)});
  }

  computeTreeDataRecursive(children, tree, parent) {
    for (const child of children) {
      tree.push({"name": child.val, "parent": parent, "nid": child.id});
      if(child.children.length > 0) {
        this.computeTreeDataRecursive(child.children, tree, child.val);
      }
    }
  }

  handleResultsClose() {this.setState({resultsDialogOpen: false});}

}

export default TagBrowser;





