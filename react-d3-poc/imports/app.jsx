import React from 'react';
import TagBrowser from '/imports/tag-browser';
import TopBar from '/imports/topbar';
import NoTagWarning from '/imports/no-tag-warning';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import update from 'react-addons-update';
import {darkBlack, grey50, indigo500, indigo50} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';

const muiTheme = getMuiTheme({
  palette: {
    textColor: darkBlack,
    primary1Color: indigo500,
    secondaryTextColor: fade(darkBlack, 0.54),
    alternateTextColor: grey50
  }
});

/**

	TODO:
	- remove node
	- clear tree
  - run query
	- change filter text color when entered

 */

class App extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			tag: null
		}
	}

	render() {

		var content = <NoTagWarning/>

		if(this.state.tag != null) {
			content = <TagBrowser tag={this.state.tag}/>
		} 

		return (
      <div className="app" style={{zIndex: 0}}>

        <MuiThemeProvider muiTheme={muiTheme}>
        	<TopBar setTag={this.setTag.bind(this)}/>
        </MuiThemeProvider>


        <MuiThemeProvider muiTheme={muiTheme}>
					{content}
        </MuiThemeProvider>

      </div>
		)
	}

  setTag(newTag) {
    var newState = update(this.state, {tag: {$set: newTag}});
    this.setState(newState);
  }

}

export default App;