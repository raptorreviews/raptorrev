import React from 'react';
import ReactDOM from 'react-dom';
import d3 from 'd3';

class D3Tree extends React.Component {

  constructor(props) {
    super(props);
    this.handleNodeClick = this.handleNodeClick.bind(this);
  }

  handleNodeClick(d) {
    this.props.selector(d);
  }

  componentDidUpdate() {
    var svg = d3.select(ReactDOM.findDOMNode(this));
    svg.selectAll("*").remove();
    renderTree(this.props.treeData, ReactDOM.findDOMNode(this), this.handleNodeClick, this.props.selected);
  }

  render() {
    return (<svg></svg>);
  }

}

var renderTree = function(treeData, svgDomNode, selector, selected) {

  var data = [];
  
  //deep copy values because I don't quite understand d3 yet
  for(var ind = 0; ind < treeData.length; ind++) {
      data.push({name:treeData[ind].name, parent:treeData[ind].parent, nid:treeData[ind].nid});
  }

    var dataMap = data.reduce(function(map, node) {
      map[node.name] = node;
      return map;
    }, {});

    var tD = [];
    data.forEach(function(node) {

      var parent = dataMap[node.parent];
      if (parent) {

        (parent.children || (parent.children = []))

          .push(node);
        } else {

        tD.push(node);
      }
    });

    var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = 960 - margin.right - margin.left,
    height = 500 - margin.top - margin.bottom;

    var i = 0;

    var tree = d3.layout.tree()
    .size([height, width]);

    var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

    var svg = d3.select(svgDomNode)
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    root = tD[0];

    update(root, selected);

    function update(source, selected) {
      var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

      nodes.forEach(function(d) { d.y = d.depth * 90; });

      var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

      var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { 
        return "translate(" + d.y + "," + d.x + ")"; });

      nodeEnter.append("circle")
      .attr("r", 8)
      .style("fill", (d) => {return (d.nid == selected ? "#5C6BC0" : "#fff")})
      .on("click", (d) => {selector(d.nid)});

      nodeEnter.append("text")
      .attr("x", function(d) { 
        return d.children || d._children ? -13 : 13; })
      .attr("dy", ".35em")
      .attr("text-anchor", function(d) { 
        return d.children || d._children ? "end" : "start"; })
      .text(function(d) { return d.name; })
      .style("fill-opacity", 1);

      var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

      link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", diagonal);

    }
  }

  export default D3Tree;


