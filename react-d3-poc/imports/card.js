import React from 'react';
import TextField from 'material-ui/TextField';
import { Card, CardHeader } from 'material-ui/Card';
import {grey500, indigo50} from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/image/edit';
import InfoOutline from 'material-ui/svg-icons/action/info-outline';
import Dialog from 'material-ui/Dialog';

class QueryCard extends React.Component {

	constructor(props) {

		super(props);

		this.state = {
			infoDialogOpen: false,
			nameChangeDialogOpen: false,
			missingFilterDialogOpen: false
		}
	}

	render() {

		return (
			<div style={{width: "100%", "marginTop": "20px"}}>			
				<Card>
			    <CardHeader title={this.props.content}/>

			    <div style={{"paddingLeft": "14px"}} >

			    	<TextField 
			    		defaultValue={this.props.filter}
			    		hintText={"Enter JQ and [ ENTER ]"}
			    		onKeyDown={this.handleFilterKeyDown.bind(this)} 
			    	/>

						<div style={{float: "right", "display": "flex"}}>
						
							<IconButton onTouchTap={this.handleNameChangeOpen.bind(this)}>
								<EditIcon color={grey500}/>
							</IconButton>

							<IconButton onTouchTap={this.handleInfoOpen.bind(this)}>
								<InfoOutline color={grey500}/>
							</IconButton>

							<div style={{paddingTop: "5px", paddingRight: "6px", paddingLeft: "6px"}}>
								<FlatButton 
									label="Add Child"
									onTouchTap={this.spawnChild.bind(this)}
								/>
								<FlatButton
									label="Add Sibling"
									primary={true}
									disabled={this.props.e}
									onTouchTap={this.props.addSibling}
								/>
							</div>
						</div>
				  </div>
				</Card>

        <Dialog
	          title={"Attributes"}
	          modal={false}
	          open={this.state.infoDialogOpen}
	          onRequestClose={this.handleInfoClose.bind(this)}>

	        	This will list all available attributes for your specified tag. <br/><br/>
	        	Source<br/>
	        	Date<br/>
	        	Location<br/>
	        	<br/>

        </Dialog>

        <Dialog
          title={"Change Node Name"}
          modal={false}
          open={this.state.nameChangeDialogOpen}
          onRequestClose={this.handleNameChangeClose.bind(this)}>

          	Name cannot be the same as any existing nodes. <br/><br/>

						<TextField 
							name="newNodeName"
							hintText="e.g. SydTerror"
							onKeyDown={this.handleDialogKeyDown.bind(this)}
						/>

        </Dialog>

        <Dialog
          title={"Missing Filter Body"}
          modal={false}
          open={this.state.missingFilterDialogOpen}
          onRequestClose={this.handleMFDClose.bind(this)}>

          	Please provide an appropriate JQ query in the text field. <br/><br/>

        </Dialog>

			</div>
		)
	}

	spawnChild() {
		if (this.props.filter != "") this.props.addChild(this.props.id);
		else this.setState({missingFilterDialogOpen: true});
	}

	handleMFDClose() {this.setState({missingFilterDialogOpen: false});}
	handleInfoClose() {this.setState({infoDialogOpen: false});}
	handleNameChangeClose() {this.setState({nameChangeDialogOpen: false});}
	handleNameChangeOpen() {this.setState({nameChangeDialogOpen: true});}

	handleInfoOpen() {
		this.props.compound(this.props.id);
		this.setState({infoDialogOpen: true});
	}

	handleDialogKeyDown(event) {
		if (event.keyCode == 13) {
			this.props.renameNode(this.props.id, event.target.value);
			this.handleNameChangeClose();
		}
	}

	handleFilterKeyDown(event) {
		if (event.keyCode == 13) {
			this.props.setFilter(this.props.id, event.target.value);
		}
	}
}

export default QueryCard;