import React from 'react';
import TextField from 'material-ui/TextField';
import {grey50} from 'material-ui/styles/colors';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import SaveIcon from 'material-ui/svg-icons/content/save';
import AddCircle from 'material-ui/svg-icons/content/add-circle';
import FontIcon from 'material-ui/FontIcon';

class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newTreeDialogOpen: false,
      newName: ''
    }
  }

  render() {

    return (

        <AppBar
          showMenuIconButton={false}
          title={
            <div style={{position: "relative"}}>
              <div style={{float: "left", marginTop: "4px", marginRight: "8px"}}>
                <FontIcon style={{fontSize: "24px"}} className={"material-icons"} color={grey50}>label</FontIcon>
              </div>
              <div>
                <span>tagtree.js</span>
              </div>
            </div>}
        >

          <IconButton disabled={true} style={{position: "absolute", top: "12%", right: "20px"}}>
            <SaveIcon color={grey50}/>
        </IconButton>

          <IconButton onTouchTap={this.handleTreeDialogOpen.bind(this)} style={{position: "absolute", top: "12%", right: "60px"}}>
            <AddCircle color={grey50}/>
        </IconButton>

          <Dialog
            title={"Add Tag"}
            modal={false}
            open={this.state.newTreeDialogOpen}
            onRequestClose={this.handleTreeDialogClose.bind(this)}
          >
            Please select a tag that has already been created.

            <br/><br/>
            <TextField 
              value={this.props.newName}
              name="DialogInput"
              hintText="e.g. Terrorism"
              onKeyDown={this.handleNewDialogKey.bind(this)} 
            />

          </Dialog>

        </AppBar>
    )
  }

  handleTreeDialogClose() {
    this.setState({newTreeDialogOpen: false});
  }

  handleTreeDialogOpen() {
    this.setState({newTreeDialogOpen: true});
  }

  handleSave(newTag) {
    this.props.setTag(newTag);
    this.handleTreeDialogClose();
  }

  handleNewDialogKey(event) {
    if (event.keyCode == 13) {
      this.handleSave(event.target.value);
    }
  }

}

export default TopBar;