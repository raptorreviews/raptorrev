# GUI Query Tree

![alt tag](http://i.imgur.com/z5I0wil.png)

## Installation

 Firstly to run the app you will need [Meteor](http://i.imgur.com/z5I0wil.png)
 
```bash
curl https://install.meteor.com/ | sh
```

Clone and run with
```sh
cd react-d3-poc
npm install
meteor
```

## Usage
Click on nodes on tree to change perspective. You can add child nodes for the currently selected node or its children.

At the moment 'Add Sibling' button only adds a sibling to Root node.

## Known Issues
Literally everything is an issue at this point.