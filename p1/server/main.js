import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
//import '../imports/api/tasks.js';

Meteor.startup(() => {
	Meteor.methods({
/*
		'fbPhotos' : function(pageID) {
			return HTTP.call("GET", "https://graph.facebook.com/"+pageID+"/photos/type=tagged");
		},*/

		'fbPhotos' : function(name, latitude, longitude) {
			var resName = encodeURI(name);
			var page = HTTP.call("GET", "https://graph.facebook.com/search?q="+resName+"&type=place&center="+latitude+","+longitude+"&access_token=979461098832111|01CJEigztSb1lpRHFy_NIv96YHo");
			return HTTP.call("GET", "https://graph.facebook.com/v2.8/"+page.data.data[0].id+"/photos?fields=images&type=uploaded&access_token=979461098832111|01CJEigztSb1lpRHFy_NIv96YHo");
		},

		'search' : function(keywords, location, cuisine, locdata) {
			var searchWord = encodeURI(keywords);
			if (locdata != null) {var c = locdata.location;}
			if (locdata != null && cuisine == "null" && location == "null") {
				return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?q="+searchWord+"&lat="+c.lat+"&lon="+c.lng, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
			} else if (locdata != null && cuisine != "null" && location == "null") {
				var zomatoc = HTTP.call("GET", "https://developers.zomato.com/api/v2.1/cuisines?city_id=270", {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				var c = zomatoc.data.cuisines;		
				console.log(c);
				cuisine = cuisine.slice(1);
				console.log(cuisine);
				num = c.length;
				var id = null;
				for (var i = 0; i < num; i++) {
					if (c[i].cuisine.cuisine_name.includes(cuisine)) {
						id = c[i].cuisine.cuisine_id;
					}
				}
				if (id == null) {
					return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?q="+searchWord+"&lat="+c.lat+"&lon="+c.lng, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				} else {
					return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?q="+searchWord+"&lat="+c.lat+"&lon="+c.lng+"&cuisines="+id, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				}
			} else if (location == "null" && cuisine == "null") {
				return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?q="+searchWord, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
			} else if (location != "null" && cuisine == "null") {
				query = encodeURI(location);
				var zomatol = HTTP.call("GET", "https://developers.zomato.com/api/v2.1/locations?query="+query, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				var zlocation = zomatol.data.location_suggestions[0];
				return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?entity_id="+zlocation.entity_id+"&entity_type="+zlocation.entity_type+"&q="+searchWord, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
			} else if (location == "null" && cuisine != "null") {
				var zomatoc = HTTP.call("GET", "https://developers.zomato.com/api/v2.1/cuisines?city_id=270", {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				var c = zomatoc.data.cuisines;		
				console.log(c);
				cuisine = cuisine.slice(1);
				console.log(cuisine);
				num = c.length;
				var id = null;
				for (var i = 0; i < num; i++) {
					if (c[i].cuisine.cuisine_name.includes(cuisine)) {
						id = c[i].cuisine.cuisine_id;
					}
				}
				if (id == null) {
					return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?q="+searchWord, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				} else {
					return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?q="+searchWord+"&cuisines="+id, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				}
			} else {
				query = encodeURI(location);
				var zomatol = HTTP.call("GET", "https://developers.zomato.com/api/v2.1/locations?query="+query, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				var zlocation = zomatol.data.location_suggestions[0];
				var zomatoc = HTTP.call("GET", "https://developers.zomato.com/api/v2.1/cuisines?city_id=270", {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				var c = zomatoc.data.cuisines;		
				console.log(c);
				cuisine = cuisine.slice(1);
				console.log(cuisine);
				num = c.length;
				var id = null;
				for (var i = 0; i < num; i++) {
					if (c[i].cuisine.cuisine_name.includes(cuisine)) {
						id = c[i].cuisine.cuisine_id;
					}
				}
				console.log(zlocation);
				if (id == null) {
					return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?entity_id="+zlocation.entity_id+"&entity_type="+zlocation.entity_type+"&q="+searchWord, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				} else {
					return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/search?entity_id="+zlocation.entity_id+"&entity_type="+zlocation.entity_type+"&q="+searchWord+"&cuisines="+id, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
				}
			}
		},


		'restaurant' : function(resID) {
			d = HTTP.call("GET", "https://developers.zomato.com/api/v2.1/restaurant?res_id="+resID, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
			return d;		
		},

		'locSearch' : function() {
			var coordinates = location().location;
			const options = {
				"user-key" : "f1756b9a9be6ecba2c87fb55895efb95",
				"Accept" : "application/json"
			}
			return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/geocode?lat="+coordinates.lat+"&lon="+coordinates.lng, options).nearby_restaurants;
		},

		'trending' : function() {
			var coordinates = location().location;
			var data = zomato(coordinates.latitude, coordinates.longitude).data;
			return data.nearby_restaurants;

		},

		'zomato' : function(latitude, longitude) {
			return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/geocode?lat="+latitude+"1&lon="+longitude, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
		},

		'location_details' : function(id, city) {
			var name = encodeURI(city);
			return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/location_details?entity_id="+id+"&entity_type="+name, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
		},

		'reviews' : function(resID) {
			return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/reviews?res_id="+resID, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
		},

		'zomato_location' : function(keyword) {
			var query = encodeURI(keyword);
			return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/locations?query="+query, {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
		},

		'location' : function() {
			return HTTP.call("POST", "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBEBIsj5wDSrav65iys8ws0DSlsAQ56HWY");
		},

		'cuisines' : function() {
			return HTTP.call("GET", "https://developers.zomato.com/api/v2.1/cuisines?city_id=270", {headers: {'user_key': '561a4733f4e548c32f6b876346d4bac5', 'Accept': 'application/json'}});
		}
		
	})
  // code to run on server at startup
});

//zomato api key: f1756b9a9be6ecba2c87fb55895efb95
//maps api key: AIzaSyBEBIsj5wDSrav65iys8ws0DSlsAQ56HWY

/*Meteor.call('flickrGetPhotos', "hello", function(err, res){
    		if (res) {
    			console.log("GOOD");
    		} else {
    			//console.log(err);
    		}
    	}); 

		
	})
  // code to run on server at startup
});*/

//zomato api key: f1756b9a9be6ecba2c87fb55895efb95
//maps api key: AIzaSyBEBIsj5wDSrav65iys8ws0DSlsAQ56HWY

/*Meteor.call('flickrGetPhotos', "hello", function(err, res){
    		if (res) {
    			console.log("GOOD");
    		} else {
    			//console.log(err);
    		}
    	}); */
