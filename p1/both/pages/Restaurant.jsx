import React from 'react';
import { Meteor } from 'meteor/meteor';
import { ImageGal } from './ImageGal.js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export var Restaurant = React.createClass({
 
  getInitialState: function() {
    return {
      details: {
        R: { res_id: 16568200 },
        apikey: '561a4733f4e548c32f6b876346d4bac5',
        id: '00000000',
        name: ' ',
        url: 'https://www.zomato.com/sydney/vapiano-cbd?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
        location: { 
          address: '.',
          locality: '.',
          city: '.',
          city_id: 260,
          latitude: '0',
          longitude: '0',
          zipcode: '0000',
          country_id: 14 
        },
        cuisines: ' ',
        average_cost_for_two: 35,
        price_range: 2,
        currency: '$',
        offers: [],
        thumb: 'https://b.zmtcdn.com/data/pictures/0/16568200/f8edc7a694df6b912611dda3dc5d8b76_featured_v2.jpg',
        user_rating: { 
          aggregate_rating: ' ',
          rating_text: 'Average',
          rating_color: 'CDD614',
          votes: '303' 
        },
        photos_url: 'n/a',
        menu_url: 'n/a',
        featured_image: 'n/a',
        has_online_delivery: 0,
        is_delivering_now: 0,
        deeplink: 'zomato://r/16568200',
        has_table_booking: 0,
        events_url: 'n/a' 
      }
      /*
      photos: [
        {
            images: [
            {
               height: 636,
               source: "https://scontent.xx.fbcdn.net/v/t1.0-9/580936_484838738215770_663167121_n.jpg?oh=4a8d1cf7c87de17a67d5b2b05b1f6a78&oe=58ADCF3C",
               width: 960
            } 
            ],
            id: "484838738215770"
        }
     ]  
     */    
    };
  }, 


  componentWillMount() {
    var resID = FlowRouter.getParam("resID");
    Meteor.call('restaurant', resID, function(error, result) {
      if(error) {
        console.log(error.reason);
      } else {
        console.log(result);
        this.setState({details: result.data});
        return;
      }
    }.bind(this));
    /*
    Meteor.call('fbPhotos', this.state.details.name, this.state.details.location.latitude, this.state.details.location.longitude, function(err, result) {
        if(err) {
          console.log(err.reason);
        } else {
          console.log(result.data.data);
          this.setState({photos: result.data.data});
          return;
        }
    }.bind(this));
*/

  },

  render() {
    return (
      /*Made 1 row and 1 col*/
      /*comments outside XML code look like this*/
      <div className="row">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
      <h2> <center> {this.state.details.name} </center> </h2>
              <div className="card">
                <div className="card-content">
                  <p> <b>Address: </b> {this.state.details.location.address} <br /> <b> Cuisine: </b> {this.state.details.cuisines}  <br /> 
                  <b> Rating: </b> {this.state.details.user_rating.aggregate_rating} / 5 <i>({this.state.details.user_rating.votes} votes) </i>
                  </p>
                </div>
              </div>
      
      <ImageGal/>
      <Reviews />
      </div>
    );
  }
});


Reviews = React.createClass({
  getInitialState:function() {
    return {
      reviews: [
      {
        review: { 
          rating: 3.5, 
          review_text: "hi", 
          id: "28335072", 
          rating_color: "9ACD32",
          review_time_friendly: "19 days ago",
          rating_text: "Good Enough",
          timestamp: 1474979867,
          likes: 0,
          user: { name: "Nancy", foodie_level: "Super Foodie", foodie_level_num: 10, foodie_color: "f58552", profile_url: "https://www.zomato.com/users/nancy-29722644?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1", profile_image: "https://b.zmtcdn.com/images/user_avatars/mug_2x.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A", profile_deeplink: "zomato://u/29722644"
          },
          comments_count: 0
        }
      },
      {
        review: { 
          rating: 3.5, 
          review_text: "hi", 
          id: "28335072", 
          rating_color: "9ACD32",
          review_time_friendly: "19 days ago",
          rating_text: "Good Enough",
          timestamp: 1474979867,
          likes: 0,
          user: { name: "Nancy", foodie_level: "Super Foodie", foodie_level_num: 10, foodie_color: "f58552", profile_url: "https://www.zomato.com/users/nancy-29722644?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1", profile_image: "https://b.zmtcdn.com/images/user_avatars/mug_2x.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A", profile_deeplink: "zomato://u/29722644"
          },
          comments_count: 0
        }
      },
      {
        review: { 
          rating: 3.5, 
          review_text: "hi", 
          id: "28335072", 
          rating_color: "9ACD32",
          review_time_friendly: "19 days ago",
          rating_text: "Good Enough",
          timestamp: 1474979867,
          likes: 0,
          user: { name: "Nancy", foodie_level: "Super Foodie", foodie_level_num: 10, foodie_color: "f58552", profile_url: "https://www.zomato.com/users/nancy-29722644?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1", profile_image: "https://b.zmtcdn.com/images/user_avatars/mug_2x.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A", profile_deeplink: "zomato://u/29722644"
          },
          comments_count: 0
        }
      },
      {
        review: { 
          rating: 3.5, 
          review_text: "hi", 
          id: "28335072", 
          rating_color: "9ACD32",
          review_time_friendly: "19 days ago",
          rating_text: "Good Enough",
          timestamp: 1474979867,
          likes: 0,
          user: { name: "Nancy", foodie_level: "Super Foodie", foodie_level_num: 10, foodie_color: "f58552", profile_url: "https://www.zomato.com/users/nancy-29722644?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1", profile_image: "https://b.zmtcdn.com/images/user_avatars/mug_2x.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A", profile_deeplink: "zomato://u/29722644"
          },
          comments_count: 0
        }
      },
      {
        review: { 
          rating: 3.5, 
          review_text: "hi", 
          id: "28335072", 
          rating_color: "9ACD32",
          review_time_friendly: "19 days ago",
          rating_text: "Good Enough",
          timestamp: 1474979867,
          likes: 0,
          user: { name: "Nancy", foodie_level: "Super Foodie", foodie_level_num: 10, foodie_color: "f58552", profile_url: "https://www.zomato.com/users/nancy-29722644?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1", profile_image: "https://b.zmtcdn.com/images/user_avatars/mug_2x.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A", profile_deeplink: "zomato://u/29722644"
          },
          comments_count: 0
        }
      }
    ]
    }; 
  },

  componentWillMount() {
    var resID = FlowRouter.getParam("resID");
    Meteor.call('reviews', resID, function(error, result) {
      if(error) {
        console.log(error.reason);
      } else {
        console.log(result);
        this.setState({reviews: result.data.user_reviews});
        return;
      }
    }.bind(this));
  },

  render() {
    return (
      <div>
      <h4> <center>Reviews</center> </h4>
      <div className="card">
        <div className="card-content">
          <p>  {this.state.reviews[0].review.review_text} <br />
          </p>
          <hr />
          <b> Rating: </b> {this.state.reviews[0].review.rating}  <br /> 
          <b> Username: </b> <a href={this.state.reviews[0].review.user.profile_url}>{this.state.reviews[0].review.user.name}</a> <br />
          <b> {this.state.reviews[0].review.review_time_friendly} </b>
        </div>
      </div>
      <div className="card">
        <div className="card-content">
          <p> {this.state.reviews[1].review.review_text}<br />
          </p>
          <hr />
          <b> Rating: </b> {this.state.reviews[1].review.rating}  <br /> 
          <b> Username: </b> <a href={this.state.reviews[1].review.user.profile_url}>{this.state.reviews[1].review.user.name}</a> <br />
          <b> {this.state.reviews[1].review.review_time_friendly} </b>
        </div>
      </div>
      <div className="card">
        <div className="card-content">
          <p> {this.state.reviews[2].review.review_text} <br />
          </p>
          <hr/>
          <b> Rating: </b> {this.state.reviews[2].review.rating}  <br /> 
          <b> Username: </b> <a href={this.state.reviews[2].review.user.profile_url}>{this.state.reviews[2].review.user.name}</a> <br />
          <b> {this.state.reviews[2].review.review_time_friendly} </b>
        </div>
      </div>
      <div className="card">
        <div className="card-content">
          <p> {this.state.reviews[3].review.review_text} <br />
          </p>
          <hr/>
          <b> Rating: </b> {this.state.reviews[3].review.rating}  <br /> 
          <b> Username: </b> <a href={this.state.reviews[3].review.user.profile_url}>{this.state.reviews[3].review.user.name}</a> <br />
          <b> {this.state.reviews[3].review.review_time_friendly} </b>
        </div>
      </div>
      <div className="card">
        <div className="card-content">
          <p> {this.state.reviews[4].review.review_text}  <br /> 
          </p>
          <hr/>
          <b> Rating: </b> {this.state.reviews[4].review.rating}  <br /> 
          <b> Username: </b> <a href={this.state.reviews[4].review.user.profile_url}>{this.state.reviews[4].review.user.name}</a> <br />
          <b> {this.state.reviews[4].review.review_time_friendly} </b>
        </div>
      </div>
      </div>
    );
  }
});

