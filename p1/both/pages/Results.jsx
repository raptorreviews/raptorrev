import React from 'react';
import { HTTP } from 'meteor/http';

export var RestaurantCard= React.createClass({

  imageExists: function(url) {
    var ima = new Image();
    ima.onLoad = function() {return true;};
    ima.onError = function() {return false;};
    ima.src = url;
  },

  render() {
    var path = "/restaurant/" + this.props.id;
    var imaurl = this.props.image
    /*if (this.imageExists(this.props.image) != true){
      imaurl = "http://www.thepapermartstore.com/media/catalog/product/cache/1/small_image/125x/9df78eab33525d08d6e5fb8d27136e95/s/q/squareblastoffblue.png";
    } */
    return (
    <div className="card">
            <div className="card-image">
               <img src={imaurl} />
               <span className="card-title">{this.props.name}</span>
            </div>
            <div className="card-content">
               <p><b>Cuisines: </b> {this.props.cuisines} <br /> 
               <b>Rating: </b> {this.props.rating} <br /> 
               </p>
            </div>
            <div className="card-action">
                <a href={path}>More Info</a>
            </div>
        </div>
        );
  }
});

export var Results = React.createClass({
  getInitialState: function() {
    return {
      locationData: {
        results_found: 1, results_start: 0, results_shown: 1,
        restaurants: [ 
        {
          restaurant: {
            R: { res_id: 16564200 },
            apikey: "f1756b9a9be6ecba2c87fb55895efb95",
            id: "16564200",
            name: "Coco Cubano",
            url: "https://www.zomato.com/sydney/coco-cubano-parramatta?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
            location: { address: "302 Church Street, Parramatta, Sydney", locality: "Parramatta", city: "Sydney", city_id: 260,
            latitude: "-33.8124873429", longitude: "151.0039043427", zipcode: "2150", country_id: 14 },
            cuisines: "Cafe, Tapas, Latin American",
            average_cost_for_two: 80,
            price_range: 3,
            currency: "$",
            offers: [],
            thumb: "https://b.zmtcdn.com/data/pictures/chains/5/16568175/5eed259e5c71018794ef99b86c5f0469_featured_v2.jpg",
            user_rating: { aggregate_rating: "2.3", rating_text: "Poor", rating_color: "FF7800", votes: "232" },
            photos_url: "https://www.zomato.com/sydney/coco-cubano-parramatta/photos#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
            menu_url: "https://www.zomato.com/sydney/coco-cubano-parramatta/menu#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
            featured_image: "https://b.zmtcdn.com/data/pictures/chains/5/16568175/5eed259e5c71018794ef99b86c5f0469_featured_v2.jpg",
            has_online_delivery: 0,
            is_delivering_now: 0,
            deeplink: "zomato://r/16564200",
            has_table_booking: 1,
            book_url: "https://www.zomato.com/sydney/coco-cubano-parramatta/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
            events_url: "https://www.zomato.com/sydney/coco-cubano-parramatta/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
            establishment_types: []
          }
        }]
      },
      loc: {
        accuracy: 0,
        location: {
          lat: 0,
          lng: 0
        }
      }
    }
  },

  componentWillMount() {
    var keyword = FlowRouter.getParam("keyword");
    var location = FlowRouter.getQueryParam("location");
    var cuisine = FlowRouter.getQueryParam("cuisine");
    var loc = Session.get('locdata');
    Meteor.call('search', keyword, location, cuisine, loc, function(err, locationString) {
        if(err) {
          console.log(err.reason);
        } else {
          console.log(locationString);
          this.setState({locationData: locationString.data});
          return;
        }
    }.bind(this));
  },

  render() {
      var keyword = FlowRouter.getParam("keyword");
      var cards = [];
      numRestaurants = this.state.locationData.results_shown;
      for (var ii = 0; ii < numRestaurants; ii++) {
        cards.push( /*get results from the api, should be in some sort of list format set each of the prop vals to vals at that index of w/e*/ 
          <RestaurantCard image={this.state.locationData.restaurants[ii].restaurant.thumb} 
          name = {this.state.locationData.restaurants[ii].restaurant.name} cuisines = {this.state.locationData.restaurants[ii].restaurant.cuisines}
          rating = {this.state.locationData.restaurants[ii].restaurant.user_rating.aggregate_rating}  
          id = {this.state.locationData.restaurants[ii].restaurant.id} />
        );
      }   
      return (
      <div>
        <h4 className="text-center">Displaying results for <i> {keyword} </i> </h4>
        <div className="cards">{cards}</div>
      </div>
      );
  }
});
