/*
** All the fancy stuff is from materialize:materialize
*/
import React from 'react';
import { Gal } from './Gal.js';
import Lightbox from 'react-images';
import { css, StyleSheet } from 'aphrodite/no-important';

export var Home = React.createClass({
  getInitialState() {
    return {showExtra: false,
            cuisine: null,
            location: null
            };
  },

  componentWillMount(){
    HTTP.call("POST", "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAKS9QfdwcKwMjf2Rp8OvBySikMFWxVAIY", function(err, l){
      if (err) {
        console.log(err.reason);
      } else {
        Session.set('locdata', l.data);
        console.log(l);
      }
    });
  },

  onSubmit(e) {
		e.preventDefault();
		let element = $(e.target);
		//grab form elements
		//let username = element.find("#username").val();
		let search = element.find("#search").val();
		//need to pass this
    console.log(this.state.location);
    console.log(this.state.cuisine);
		FlowRouter.go('/results/'+search+"?cuisine="+this.state.cuisine+"&location="+this.state.location);
	},

  getCuisine(e) {
    this.setState({cuisine: e.target.value});
  },
   getLocation(e) {
    this.setState({location: e.target.value});
  },
  adv() {
    console.log("set state showextra");
    this.setState({showExtra: true});
  },

  render() {
    var navStyle = {
      backgroundColor: "#ffffff",
      color: "#000000",
      paddingLeft: "12px"
    };
    return (
      /*Made 1 row and 1 col*/
      /*comments outside XML code look like this*/
      <div className="row">
      <br/>
      <h4 className="text-center">Welcome to Local Bites!</h4>
      <h5 className="text-center">Begin your search below</h5>
      <br/>

      <div className="row">
        <nav style={navStyle}>
         <div className="nav-wrapper">
           <form onSubmit={this.onSubmit}>
             <div className="input-field">
               <input id="search" type="search" required/>
               <label htmlFor="search"><i className="material-icons">search</i></label>
               <i className="material-icons">close</i>
             </div>
              { this.state.showExtra ? <Extra getCuisine = {this.getCuisine}  getLocation = {this.getLocation}/> : null }
             <button className="waves-effect waves-light btn btn-block" onSubmit={this.onSubmit}>Search</button>
           </form>
           { this.state.showExtra ? null :  <button className="waves-effect waves-light btn btn-block" onClick={this.adv}>Advanced</button>}
         </div>
       </nav>
       <br/>
      </div>
      </div>
       );
  }
});

var Extra = React.createClass({
  // console.log("rendering extra");  
   render: function() {
    return(
      <div>
        <label htmlFor="cuisine"> Cuisine </label> 
        <input type="text" onChange={this.props.getCuisine} /> 
        <br />
        <label htmlFor="location"> Place </label> 
        <input  type = "text" onChange={this.props.getLocation}  /> 
      </div>
    );
  }
})