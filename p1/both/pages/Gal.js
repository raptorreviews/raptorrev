import React from 'react';
import Lightbox from 'react-images';
import { css, StyleSheet } from 'aphrodite/no-important';

export var Gal = React.createClass ({
  getInitialState() {
    return{
      lightboxIsOpen: false,
      currentImage: 0,
    }
  },

  openLightbox (index, event) {
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightboxIsOpen: true,
    });
  },

  closeLightbox () {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  },

  gotoPrevious () {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  },

  gotoNext () {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  },

  gotoImage (index) {
    this.setState({
      currentImage: index,
    });
  },

  handleClickImage () {
    if (this.state.currentImage === this.props.images.length - 1) return;

    this.gotoNext();
  },

  renderGallery () {
    const { images } = [{ src: 'http://www.intrepidtravel.com/sites/default/files/mexico_food_Quesadilla_0_0.jpg' }, { src: 'http://www.intrepidtravel.com/sites/default/files/mexico_food_Quesadilla_0_0.jpg' }];

    if (!images) return;

    const gallery = images.filter(i => i.useForDemo).map((obj, i) => {
      return (
        <a
          href={obj.src}
          className={css(classes.thumbnail, classes[obj.orientation])}
          key={i}
          onClick={(e) => this.openLightbox(i, e)}
        >
          <img src={obj.thumbnail} className={css(classes.source)} />
        </a>
      );
    });

    return (
      <div className={css(classes.gallery)}>
        {gallery}
      </div>
    );
  },

  render() {
    return (
      <div className="section">
        {this.renderGallery()}
        Hi why arent u working hunty<br/>
        <Lightbox
          currentImage={this.state.currentImage}
          images={[{ src: 'http://www.intrepidtravel.com/sites/default/files/mexico_food_Quesadilla_0_0.jpg' }, { src: 'http://www.intrepidtravel.com/sites/default/files/mexico_food_Quesadilla_0_0.jpg' }]}
          isOpen={this.state.lightboxIsOpen}
          onClickImage={this.handleClickImage}
          onClickNext={this.gotoNext}
          onClickPrev={this.gotoPrevious}
          onClose={this.closeLightbox} />
      </div>

    );
  }
});