import React from 'react';
//import ImageGallery from 'react-image-gallery';
var Slider = require('react-slick');

export var ImageGal = React.createClass({
  
  getInitialState: function() {
    return {
      photos: [
        {
            images: [
            {
               height: 636,
               source: "http://www.iconsdb.com/icons/preview/white/square-rounded-xxl.png",
               width: 960
            } 
            ],
            id: "484838738215770"
        }
     ]
   }
  },

  componentWillMount(){
    var resID = FlowRouter.getParam("resID");
    var latitude, longitude, name = "";
    Meteor.call('restaurant', resID, function(error, result) {
      if(error) {
        console.log(error.reason);
      } else {
        console.log(result);
        latitude = result.data.location.latitude;
        longitude = result.data.location.longitude;
        name = result.data.name
        Meteor.call('fbPhotos', name, latitude, longitude, function(err, result) {
          if(err) {
            console.log(err.reason);
          } else {
            console.log(result.data.data);
            this.setState({photos: result.data.data});
            return;
          }
         }.bind(this));
        return;
      }
    }.bind(this));
  
  },

  render() {
    var url = "http://i.stack.imgur.com/S5Jxg.png"
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      vertical: true,
      verticalSwiping: true,
      swipeToSlide: true,
      beforeChange: function (currentSlide, nextSlide) {
        console.log('before change', currentSlide, nextSlide);
      },
      afterChange: function (currentSlide) {
        console.log('after change', currentSlide);
      }
    };
    var cards =[]
    var pList = this.state.photos
    var items = this.state.photos.length;
    photoString = "";
    for (var j = 0; j < items; j++) {
      var size = this.state.photos[j].images.length - 1;
      cards.push(<PhotoCard source={this.state.photos[j].images[size].source} />);
    }
    return (
      <div>
        <h4> <center>Images</center> </h4>
        <Slider {...settings}>
          <div><img src="http://www.iconsdb.com/icons/preview/white/square-rounded-xxl.png" /></div>
          {cards}
        </Slider>
      </div>
    );
  }
});

var PhotoCard = React.createClass({
  render() {
    return (
      <div><center><img src ={this.props.source} height={this.props.height} width={this.props.width}/></center></div>
    );
  }
});

