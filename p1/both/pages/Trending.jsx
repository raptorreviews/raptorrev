import React from 'react';
import { RestaurantCard } from './Results.jsx';

export var Trending = React.createClass({
  render() {
    return (
    	//id image name cuisines rating
       <div className="row">
        <h4 className="text-center">Trending Restaurants in Your Area</h4>
        <div className="col s12">
          <div className="row">
            <div className="input-field col s6">
              <RestaurantCard image={this.state.locationData.restaurants[ii].restaurant.thumb} 
              name = {this.state.locationData.restaurants[ii].restaurant.name} cuisines = {this.state.locationData.restaurants[ii].restaurant.cuisines}
              rating = {this.state.locationData.restaurants[ii].restaurant.user_rating.aggregate_rating}  
              id = {this.state.locationData.restaurants[ii].restaurant.id} />
            </div>
            <div className="input-field col s6">
                <RestaurantCard image={this.state.locationData.restaurants[ii].restaurant.thumb} 
              name = {this.state.locationData.restaurants[ii].restaurant.name} cuisines = {this.state.locationData.restaurants[ii].restaurant.cuisines}
              rating = {this.state.locationData.restaurants[ii].restaurant.user_rating.aggregate_rating}  
              id = {this.state.locationData.restaurants[ii].restaurant.id} />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s6">
              <RestaurantCard image={this.state.locationData.restaurants[ii].restaurant.thumb} 
              name = {this.state.locationData.restaurants[ii].restaurant.name} cuisines = {this.state.locationData.restaurants[ii].restaurant.cuisines}
              rating = {this.state.locationData.restaurants[ii].restaurant.user_rating.aggregate_rating}  
              id = {this.state.locationData.restaurants[ii].restaurant.id} />
            </div>
            <div className="input-field col s6">
              <div className="card">
                <div className="card-image">
                  <img src="http://sawadacoffee.com/wp-content/uploads/Sawada-Coffee-10DEC2015-003.jpg" />
                  <span className="card-title">The Whitehouse</span>
                </div>
                <div className="card-content">
                  <p>Weatherboard bar/restaurant with a beer garden, dispensing breakfast, pizzas and wraps, plus gigs.</p>
                </div>
                <div className="card-action">
                  <a href="http://www.arc.unsw.edu.au/eat/whitehouse">website</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

export var trendCards = React.createClass({
	getInitialState: function() {
    return {
      photos: [
        {
            images: [
            {
               height: 636,
               source: "http://www.iconsdb.com/icons/preview/white/square-rounded-xxl.png",
               width: 960
            } 
            ],
            id: "484838738215770"
        }
     ]
   }
  },

  componentWillMount(){
   Meteor.call('fbPhotos', this.props.name, this.props.latitude, this.props.longitude, function(err, result) {
   	if(err) {
   		console.log(err.reason);
    } else {
    	console.log(result.data.data);
    	this.setState({photos: result.data.data});
    	return;
    }
	}.bind(this));
  
  },	

  render() {
    return (
      <div className='NotFound'>
        <h1>This page can not be found</h1>
      </div>
    );
  }

});
