// Reaktor API:  https://github.com/kadirahq/meteor-reaktor
// Router API:   https://github.com/meteorhacks/flow-router

// NOTE see flow-router branch for the vanilla router

/*
Reaktor.init(
  <Router>
    <Route path="/" content={Home} layout={MainLayout} />
    <Route path="/register" content={Register} layout={MainLayout} triggersEnter={isLoggedIn} />
    <Route path="/login" content={Login} layout={MainLayout} triggersEnter={isLoggedIn} />
	  <Route path="/results" content={Results} layout={MainLayout} />
    <Route path="/profile" content={Profile} layout={MainLayout} />
  </Router>
);

*/
import React from 'react';
import {mount} from 'react-mounter';
import {Results} from './pages/Results.jsx';
import {MainLayout} from './pages/MainLayout.jsx';
import {Home} from './pages/Home.jsx';
import {NotFound} from './pages/NotFound.jsx';
import './models/user.js';
import {Register} from './components/profile/Register.jsx';
import {Login} from './components/profile/Login.jsx';
import {Profile} from './components/profile/profile.jsx';
import {Restaurant} from './pages/Restaurant.jsx';

//checks if logged in
function isLoggedIn(context, doRedirect) {
  if(User.isLoggedIn()) {
    //if user is logged in, buttons will redirect to homepage
    doRedirect('/')
  }
}

FlowRouter.route("/",  {
  action() {
    mount(MainLayout, { content: <Home /> });
  }
});

FlowRouter.route("/register",  {
  triggersEnter: [isLoggedIn],
  action() {
    mount(MainLayout, { content: <Register /> });
  }
});

FlowRouter.route("/login",  {
  triggersEnter: [isLoggedIn],
  action() {
    mount(MainLayout, { content: <Login /> });
  }
});

FlowRouter.route("/results/:keyword",  {
  action: function(params, queryParams) {
    console.log(params);
    mount(MainLayout, { content: <Results /> });
  }
});

FlowRouter.route("/profile",  {
  action() {
    mount(MainLayout, { content: <Profile /> });
  }
});

FlowRouter.route("/restaurant/:resID",  {
  action: function(params, queryParams) {
    console.log(params)
    mount(MainLayout, { content: <Restaurant /> });
  }
});

FlowRouter.notFound = {
  action() {
    mount(MainLayout, { content: <NotFound /> });
  }
};
/*
// Reaktor doensn't have a notFound component yet
FlowRouter.notFound = {
  action() {
    ReactLayout.render(MainLayout, { content: <NotFound /> });
  }
};
*/