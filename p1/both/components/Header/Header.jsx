import React from 'react';
import AccountsWrapper from './AccountsWrapper.jsx';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

Header = React.createClass({
  getInitialState() {
    return {
      isLoggedIn: User.isLoggedIn()
    };
  },
  logout() {
    Meteor.logout((error) => {
      if(error) {
				Materialize.toast(error.reason, 4000);
			} else {
        this.setState({isLoggedIn: !this.state.isLoggedIn});
				FlowRouter.go('/');
			}
    }).bind(this);
  },
  render() {
    var navStyle = {
      backgroundColor: "#0FA3B1",
      paddingLeft: "12px"
    };
    var navOptions = User.isLoggedIn() ? <LoggedInNav logout={this.logout} /> : <LoggedOutNav />;
    return (
      <nav style={navStyle}>
        <div className="nav-wrapper">
          <a href="/" className="brand-logo center">Local Bites</a>
          {navOptions}
        </div>
      </nav>

    );
  }
});
