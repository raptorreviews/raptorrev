import React from 'react';

LoggedInNav = React.createClass ({
  render() {
    return (
      <ul id="nav-mobile" className="right hide-on-med-and-down">
        <li><a href="/">Home</a></li>
        {/*a child was passed in holding the logout fn. it can be ref by this.props*/}
        <li><a href="/profile">Profile</a></li>
        <li><a onClick={this.props.logout}>Logout</a></li>
      </ul>
    );
  }
});
