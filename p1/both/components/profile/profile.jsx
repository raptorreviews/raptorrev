import React from 'react';

export var Profile = React.createClass({
  render() {
    var x = User.get();
    console.log(x);
    return (
      <div className="row">
        <h2 className="text-center">Profile Page</h2>
        {/*<img className="circle responsive-img" src="https://pbs.twimg.com/profile_images/562466745340817408/_nIu8KHX.jpeg"/>*/}
        <div className="col s6 m8 offset-m2 l6 offset-l3">
         <div className="card-panel grey lighten-5 z-depth-1">
           <div className="row valign-wrapper">
             <div className="col s6">
               <img src="https://pbs.twimg.com/profile_images/562466745340817408/_nIu8KHX.jpeg" alt="" className="circle responsive-img"/>
               <br/><b>Email: </b> k.suvercha@gmail.com <br/>
             </div>
            </div>
           </div>
         </div>
        </div>

   );
  }
});
