import React from 'react';

export var Login = React.createClass({
  onSubmit(e) {
		e.preventDefault();

		let element = $(e.target);
		//grab form elements
		//let username = element.find("#username").val();
		let email = element.find("#email").val();
		let password = element.find("#password").val();
		//do some checks before submitting
    Meteor.loginWithPassword(email, password, (error) => {
			if(error) {
				Materialize.toast(error.reason, 4000);
			} else {
				//Redirect
				FlowRouter.go('/');
			}
    });
	},
	render() {
		return (
			<div>
      <div className="row">
        <h4 className="text-center">Login</h4>
        <form onSubmit={this.onSubmit} className="col offset-s4 s4">
          <div className="row">
            <div className="input-field col s12">
              <input id="email" type="text" className="validate" />
              <label htmlFor="email">Email</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <input id="password" type="password" className="validate" />
              <label htmlFor="password">Password</label>
            </div>
          </div>
          <div className="row">
            <button className="waves-effect waves-light btn btn-block">Submit</button>
          </div>
        </form>
      </div>
      </div>
		);
	}
});
